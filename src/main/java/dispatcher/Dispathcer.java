package dispatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import javax.servlet.annotation.WebServlet;


@WebServlet("/")
public class Dispathcer extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Boolean login = (Boolean) req.getSession().getAttribute("login");
        if (!login) {
            //if the user is not login, we reddirect him to the login servlet
            req.getRequestDispatcher("/login").forward(req, resp);
        } else {
            //if there is a note, we get it from post.
            String note = req.getParameter("note");
            if (note != null) {
                //if there's a note, we redirect to the addNote servlet
                req.getRequestDispatcher("/addnote").forward(req, resp);
            } else {
                //we redirect to default servlet handler
                req.getRequestDispatcher("/default").forward(req, resp);
            }
        }
    }
}
